﻿using UnityEngine;
using System.IO;
using System.Collections;

namespace PuzzleMaker
{
    public class WebPmFileLoader : MonoBehaviour
    {

        [HideInInspector]
        public Stream PMFileStream = null;


        public IEnumerator LoadFileToStream(string PMFilePath)
        {
            
            PMFileStream = null;

#if !UNITY_5_6_OR_NEWER
            if (Application.platform == RuntimePlatform.WebGLPlayer ||
                Application.platform == RuntimePlatform.WindowsWebPlayer)
#else
            if (Application.platform == RuntimePlatform.WebGLPlayer)
#endif
            {

                string FileName = System.IO.Path.GetFileName(PMFilePath);
                string FilePath = Application.streamingAssetsPath + "/" + FileName;

#if !UNITY_5_6_OR_NEWER
                if (Application.platform == RuntimePlatform.WindowsWebPlayer)
                    FilePath = Application.streamingAssetsPath.Replace("Raw", "StreamingAssets") + "/" + FileName;
#endif


                WWW www = new WWW(FilePath);

                yield return www;



                if (string.IsNullOrEmpty(www.error))
                {
                    PMFileStream = new System.IO.MemoryStream(www.bytes);
                    Debug.Log("Loaded bytes length : " + www.bytes.Length);
                    Debug.Log("Web file loading completed");
                }

            }

        }

    }
}
