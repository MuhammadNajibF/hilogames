﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scenemanager : MonoBehaviour
{

    public Animation Transition;
    public string[] Scene;

    public void StartScene() 
    {
        SceneManager.LoadScene(Scene[0]);
        //StartCoroutine(Scene());
    }

    

    public void PindahSceneDice()
    {
        SceneManager.LoadScene(Scene[1]);
        //StartCoroutine(Scene());
    }

    public void PindahScenePuzzle()
    {
        SceneManager.LoadScene(Scene[2]);
        //StartCoroutine(Scene());
    }

    public void PindahSceneScan()
    {
        SceneManager.LoadScene(Scene[3]);
        //StartCoroutine(Scene());
    }
    
    //IEnumerator Scene() 
    //{
    //    //Transition.Play();
    //    //yield return new WaitForSeconds(1.5f);
    //    //SceneManager.LoadScene("ExamplesTakeColor");
    //}

    public void BalikScene()
    {
        SceneManager.LoadScene("Home");
    }
}
