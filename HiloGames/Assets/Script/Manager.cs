﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{

    public RenderTextureCamera2 R_Camera;
    public ScreenOrientationManager S_OrientationManager;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SceneHiloBook1() 
    {
        StartCoroutine("SceneHiloBook1Coroutine");
    }

    IEnumerator SceneHiloBook1Coroutine() 
    {
        yield return new WaitForSeconds(2f); 
        R_Camera.MakeScreen();


        yield return new WaitForSeconds(2f);
        S_OrientationManager.LandscapeOrientation();
        SceneManager.LoadScene("Puzzle");
    }
}
