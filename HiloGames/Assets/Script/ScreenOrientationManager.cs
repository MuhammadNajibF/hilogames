﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenOrientationManager : MonoBehaviour
{
    public static ScreenOrientation orientation;

    
    //For LandscapeOrientation
    public void LandscapeOrientation() 
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }

    //For PotraitOrientation
    public void PotraitOrientation() 
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }
}
